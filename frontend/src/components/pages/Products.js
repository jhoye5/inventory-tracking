import React from 'react'
import BasicTable from '../BasicTable.js'

function Products() {
    return (
        <div className='products'>
            <h1>Products</h1>   
            <br />
            <br />      
            <BasicTable />   
        </div>
    )
}

export default Products